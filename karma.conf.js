/* eslint-env node */
/* eslint-disable angular/log */

process.env.NODE_ENV = process.env.NODE_ENV || 'test';

var webpackConfig = require('./webpack.config');
var path = require('path');
var _ = require('lodash');


var coverage = process.env.COVERAGE === 'true';
if (coverage) {
	console.log('-- recording coverage --');
}

var runDaemon = _.isBoolean(process.env.CI) ? process.env.CI :  process.env.DEBUG;

var files = [
];
if (_.isArray(webpackConfig.entry)) {
	files.push(_.transform(webpackConfig.entry, function (result, value) {
		var file = path.join(webpackConfig.context, value);
		result.push(file);
	}, []));
} else {
	files.push(path.join(webpackConfig.context, webpackConfig.entry));
}

var vendor = [
	'node_modules'
];

module.exports = function (config) {
	var report = './report';
	var options = {
		// base path that will be used to resolve all patterns (eg. files, exclude)
		basePath: './',

		// frameworks to use
		// some available frameworks: https://npmjs.org/browse/keyword/karma-adapter
		frameworks: ['jasmine'],

		// list of files / patterns to load in the browser
		files: files,

		// list of files to exclude
		exclude: [],

		proxies: {},

		// preprocess matching files before serving them to the browser
		// available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
		preprocessors: {
			'test/index.js': ['webpack', 'sourcemap']
		},

		babelPreprocessor: {
			options: {
				modules: 'system'
			}
		},

		webpack: webpackConfig,

		webpackMiddleware: {
			noInfo: true
		},

		// test results reporter to use
		// possible values: 'dots', 'progress', 'coverage'
		// available reporters: https://npmjs.org/browse/keyword/karma-reporter
		reporters: ['progress', 'verbose', 'coverage'],

		coverageReporter: {
			dir: report,
			reporters: [
				// reporters not supporting the `file` property
				{type: 'html', subdir: 'report-html'},
				{type: 'lcov', subdir: 'report-lcov'},
				{type: 'text-summary'} //, subdir: '.', file: 'text-summary.txt'}
			]
		},

		// web server port
		port: 9876,

		// enable / disable colors in the output (reporters and logs)
		colors: true,

		// level of logging
		// possible values: config.LOG_DISABLE || config.LOG_ERROR ||
		// config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
		logLevel: config.LOG_INFO,

		// enable / disable watching file and executing tests whenever any file changes
		autoWatch: runDaemon,

		// start these browsers
		// available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
		//        browsers: ['Chrome', 'ChromeCanary', 'FirefoxAurora', 'Safari', 'PhantomJS'],
		browsers: ['Chrome'],

		// Continuous Integration mode
		// if true, Karma captures browsers, runs the tests and exits
		singleRun: !runDaemon
	};

	config.set(options);
};
