/*
 * Copyright (c) 2016. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : debox-web-core
 *  Module     : debox-web-core
 *  File       : http-auth-interceptor.js
 *  ClassName  : http-auth-interceptor.js
 *  Modified   : 16012817
 */
import angular from 'angular';


let httpAuthInterceptorModule = angular.module('httpAuthInterceptor', ['httpAuthInterceptorBuffer'])
	.constant('AUTH_EVENTS', {
		loginSuccess: 'auth-login-success',
		loginFailed: 'auth-login-failed',
		logoutSuccess: 'auth-logout-success',
		sessionTimeout: 'auth-session-timeout',
		notAuthenticated: 'auth-not-authenticated',
		notAuthorized: 'auth-not-authorized'
	})
	.factory('authService', authService)
	.config(authConfig);


/* @ngInject */
function authService($rootScope, httpBuffer, AUTH_EVENTS) {
	return {
		loginSuccess: loginSuccess,
		loginFailed: loginFailed,
		logoutSuccess: logoutSuccess
	};

	/**
	 * logout handler
	 */
	function logoutSuccess() {
		$rootScope.$broadcast(AUTH_EVENTS.logoutSuccess);
	}

	/**
	 * Call this function to indicate that authentication was successfull and trigger a
	 * retry of all deferred requests.
	 * @param data an optional argument to pass on to $broadcast which may be useful for
	 * example if you need to pass through details of the user that was logged in
	 * @param configUpdater an optional transformation function that can modify the
	 * requests that are retried after having logged in.  This can be used for example
	 * to add an authentication token.  It must return the request.
	 */
	function loginSuccess(data, configUpdater) {
		var updater = configUpdater || function (config) {
				return config;
			};
		$rootScope.$broadcast(AUTH_EVENTS.loginSuccess, data);
		httpBuffer.retryAll(updater);
	}

	/**
	 * Call this function to indicate that authentication should not proceed.
	 * All deferred requests will be abandoned or rejected (if reason is provided).
	 * @param data an optional argument to pass on to $broadcast.
	 * @param reason if provided, the requests are rejected; abandoned otherwise.
	 */
	function loginFailed(data, reason) {
		httpBuffer.rejectAll(reason);
		$rootScope.$broadcast(AUTH_EVENTS.loginFailed, data);
	}
}
/**
 * $http interceptor.
 * On 401 response (without 'ignoreAuthModule' option) stores the request
 * and broadcasts AUTH_EVENTS.notAuthenticated.
 * On 403 response (without 'ignoreAuthModule' option) discards the request
 * and broadcasts AUTH_EVENTS.notAuthorized.
 * @ngInject
 */
function authConfig($httpProvider) {
	$httpProvider.interceptors.push(function ($rootScope, $q, httpBuffer, $timeout, AUTH_EVENTS) {
		return {
			responseError: function (rejection) {
				var config = rejection.config || {};
				if (!config.ignoreAuthModule) {
					switch (rejection.status) {
						case 401:
							var deferred = $q.defer();
							if (httpBuffer.isEmpty()) {
								$timeout(function () {
									$rootScope.$broadcast(AUTH_EVENTS.notAuthenticated, rejection);
								});
							}
							httpBuffer.append(config, deferred);
							return deferred.promise;
						case 403:
							$rootScope.$broadcast(AUTH_EVENTS.notAuthorized, rejection);
							break;
					}
				}
				// otherwise, default behaviour
				return $q.reject(rejection);
			}
		};
	});
}
/**
 * Private module, a utility, required internally by 'http-auth-interceptor'.
 */
angular.module('httpAuthInterceptorBuffer', [])
	.factory('httpBuffer', httpBufferFactory);

/* @ngInject */
var httpBufferFactory = function ($injector) {
	/** Holds all the requests, so they can be re-requested in future. */
	var buffer = [];

	/** Service initialized later because of circular dependency problem. */
	var $http;

	function retryHttpRequest(config, deferred) {
		function successCallback(response) {
			deferred.resolve(response);
		}

		function errorCallback(response) {
			deferred.reject(response);
		}

		$http = $http || $injector.get('$http');
		$http(config).then(successCallback, errorCallback);
	}

	return {
		/**
		 * Appends HTTP request configuration object with deferred response attached to buffer.
		 */
		append: function (config, deferred) {
			buffer.push({
				config: config,
				deferred: deferred
			});
		},

		/**
		 * Abandon or reject (if reason provided) all the buffered requests.
		 */
		rejectAll: function (reason) {
			if (reason) {
				for (var i = 0; i < buffer.length; ++i) {
					buffer[i].deferred.reject(reason);
				}
			}
			buffer = [];
		},

		/**
		 * Retries all the buffered requests clears the buffer.
		 */
		retryAll: function (updater) {
			for (var i = 0; i < buffer.length; ++i) {
				retryHttpRequest(updater(buffer[i].config), buffer[i].deferred);
			}
			buffer = [];
		},

		isEmpty: function () {
			return buffer.length === 0;
		}
	};
};

export default httpAuthInterceptorModule;
