/*
 * Copyright (c) 2016. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : debox-web-core
 *  Module     : debox-web-core
 *  File       : debox-core.module.js
 *  ClassName  : debox-core.module.js
 *  Modified   : 16049210
 */

import angular from 'angular';
import toastr from 'toastr';
import deboxCoreProvider from './debox-core.provider';
import './auth/http-auth-interceptor.module';
import  './exception/exception.module';
import './logger/logger.module';
import './router/router.module';
import './session/session.module';

export default angular
	.module('deboxCore', [
        'ngAnimate',
        'ngSanitize',
        'ui.router',
        'ui.bootstrap',
        //'ngplus',
        'httpAuthInterceptor',
        'deboxCore.exception',
        'deboxCore.logger',
        'deboxCore.router'])
	
    .provider('deboxCore', deboxCoreProvider);
