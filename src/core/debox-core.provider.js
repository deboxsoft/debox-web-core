/*
 * Copyright (c) 2016. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : debox-web-core
 *  Module     : debox-web-core
 *  File       : debox-core.provider.js
 *  ClassName  : debox-core.provider.js
 *  Modified   : 16012817
 */

import angular from 'angular';

/* @ngInject */
export default function deboxCoreProvider($document) {

	var contextPath = '/',
        imagesPath = 'assets/images/',
        fontsPath = 'assets/fonts/',
        cssPath = 'assets/css/',
        libsPath = 'assets/libs/';

    this.register = _register;

    this.$get = {
        isTouchDevice: _checkTouchDevice,
        getUniqueID: _uniqueID,
        getViewPort: _getViewPort,
        getCssPath: _getCssPath,
        getContextPath: _getContextPath,
        getImagesPath: _getImagesPath,
        getLibsPath: _getLibsPath,
        getFontsPath: _getFontsPath
    };
    angular.extend(this, this.$get);

    //////////////////////

    function _register(name, fn) {
    }

    // check for device touch support
    function _checkTouchDevice() {
        try {
            $document.createEvent('TouchEvent');
            return true;
        } catch (e) {
            return false;
        }
    }

    function _uniqueID(prefix) {
        return prefix + '_' + Math.floor(Math.random() * (new Date()).getTime());
    }

    // To get the correct viewport width based on  http://andylangton.co.uk/articles/javascript/get-viewport-size-javascript/
    function _getViewPort() {
        var e = window,
            a = 'inner';
        if (!('innerWidth' in window)) {
            a = 'client';
            e = $document.documentElement || $document.body;
        }

        return {
            width: e[a + 'Width'],
            height: e[a + 'Height']
        };
    }

    function _getContextPath() {
        return contextPath;
    }

    function _getCssPath(isAbsolute) {
        return isAbsolute === true ? _getContextPath() + cssPath : cssPath;
    }

    function _getImagesPath(isAbsolute) {
        return isAbsolute === true ? _getContextPath() + imagesPath : imagesPath;
    }

    function _getLibsPath(isAbsolute) {
        return isAbsolute === true ? _getContextPath() + libsPath : libsPath;
    }

    function _getFontsPath(isAbsolute) {
        return isAbsolute === true ? _getContextPath() + fontsPath : fontsPath;
    }
}

