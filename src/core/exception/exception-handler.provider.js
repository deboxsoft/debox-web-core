/*
 * Copyright (c) 2016. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : debox-web-core
 *  Module     : debox-web-core
 *  File       : exception-handler.provider.js
 *  ClassName  : exception-handler.provider.js
 *  Modified   : 16012817
 */

// Include in index.html so that dbx level exceptions are handled.


/**
 * Must configure the exception handling
 * @return {[type]}
 */
function exceptionHandlerProvider() {
    /* jshint validthis:true */
    var config = this.config = {
        deboxErrorPrefix: undefined
    };

    this.configure = function (deboxErrorPrefix) {
        this.config.deboxErrorPrefix = deboxErrorPrefix;
    };

    this.$get = function () {
        return {config: config};
    };
}


/**
 * Configure by setting an optional string value for dbxErrorPrefix.
 * Accessible via config.dbxErrorPrefix (via config value).
 * @param  {[type]} $provide
 * @return {[type]}
 * @ngInject
 */
export function config($provide) {
    $provide.decorator('$exceptionHandler', extendExceptionHandler);
}


/**
 * Extend the $exceptionHandler service to also display a toast.
 * @param  {Object} $delegate
 * @param  {Object} exceptionHandler
 * @param  {Object} logger
 * @return {Function} the decorated $exceptionHandler service
 * @ngInject
 */
function extendExceptionHandler($delegate, exceptionHandler, logger) {
    return function (exception, cause) {
        var dbxErrorPrefix = exceptionHandler.config.deboxErrorPrefix || '';
        var errorData = {exception: exception, cause: cause};
        exception.message = dbxErrorPrefix + exception.message;
        $delegate(exception, cause);
        /**
         * Could add the error to a service's collection,
         * add errors to $rootScope, log errors to remote web server,
         * or log locally. Or throw hard. It is entirely up to you.
         * throw exception;
         *
         * @example
         *     throw { message: 'error message we added' };
         */
        logger.error(exception.message, errorData);
    };
}

export default exceptionHandlerProvider;