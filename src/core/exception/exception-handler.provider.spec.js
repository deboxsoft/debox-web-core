/*
 * Copyright (c) 2016. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : debox-web-core
 *  Module     : debox-web-core
 *  File       : exception-handler.provider.spec.js
 *  ClassName  : exception-handler.provider.spec.js
 *  Modified   : 16012817
 */

/* jshint -W117, -W030 */
import './exception.module';
import 'angular-mocks';

describe('deboxCore.exception', function () {
    var exceptionHandlerProvider, $rootScope, $httpBackend;
    var mocks = {
        errorMessage: 'fake error',
        prefix: '[TEST]: '
    };

    beforeEach(function () {
        angular.mock.module('deboxCore.exception', function (_exceptionHandlerProvider_) {
            exceptionHandlerProvider = _exceptionHandlerProvider_;
        });
        inject(function (_$rootScope_, _$httpBackend_) {
            $rootScope = _$rootScope_;
            $httpBackend = _$httpBackend_;
        });
    });

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('$exceptionHandler', function () {
        it('should have a dummy test', inject(function () {
            expect(true).toBeTruthy();
        }));

        it('should be defined', inject(function (_exceptionHandler_) {
            expect(_exceptionHandler_).toBeDefined();
        }));

        it('should have configuration', inject(function (_exceptionHandler_) {
            expect(_exceptionHandler_.config).toBeDefined();
        }));

        describe('with deboxErrorPrefix', function () {
            beforeEach(function () {
                exceptionHandlerProvider.configure(mocks.prefix);
            });

            it('should have exceptionHandlerProvider defined', inject(function () {
                expect(exceptionHandlerProvider).toBeDefined();
            }));

            it('should have deboxErrorPrefix defined', inject(function () {
                expect(exceptionHandlerProvider.$get().config.deboxErrorPrefix).toBeDefined();
            }));

            it('should have deboxErrorPrefix set properly', inject(function () {
                expect(exceptionHandlerProvider.$get().config.deboxErrorPrefix)
                    .toEqual(mocks.prefix);
            }));

            it('should throw an error when forced', inject(function () {
                expect(functionThatWillThrow).toThrow();
            }));

            it('manual error is handled by decorator', function () {
                var exception;
                exceptionHandlerProvider.configure(mocks.prefix);
                try {
                    $rootScope.$apply(functionThatWillThrow);
                }
                catch (ex) {
                    exception = ex;
                    expect(ex.message).toEqual(mocks.prefix + mocks.errorMessage);
                }
            });
        });
    });

    function functionThatWillThrow() {
        throw new Error(mocks.errorMessage);
    }
});
