/*
 * Copyright (c) 2016. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : debox-web-core
 *  Module     : debox-web-core
 *  File       : exception.factory.js
 *  ClassName  : exception.factory.js
 *  Modified   : 16012817
 */

/* @ngInject */
export default function exceptionFactory($q, logger) {
    return {
        catcher: catcher
    };

    ////////////////

    function catcher(message) {
        return function (e) {
            var thrownDescription;
            var newMessage;
            if (e.data && e.data.description) {
                thrownDescription = '\n' + e.data.description;
                newMessage = message + thrownDescription;
            }
            e.data.description = newMessage;
            logger.error(newMessage);
            return $q.reject(e);
        };

    }
}
