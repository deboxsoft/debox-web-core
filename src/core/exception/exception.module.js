/*
 * Copyright (c) 2016. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : debox-web-core
 *  Module     : debox-web-core
 *  File       : exception.module.js
 *  ClassName  : exception.module.js
 *  Modified   : 16012817
 */

import angular from 'angular';
import '../logger/logger.module';
import ExceptionHandlerProvider from './exception-handler.provider';
import {config} from './exception-handler.provider';
import exceptionFactory from './exception.factory';

export default angular
    .module('deboxCore.exception', ['deboxCore.logger'])
    .provider('exceptionHandler', ExceptionHandlerProvider)
    .config(config)
    .factory('exception', exceptionFactory);

