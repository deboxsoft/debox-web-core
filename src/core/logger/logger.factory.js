/*
 * Copyright (c) 2016. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : debox-web-core
 *  Module     : debox-web-core
 *  File       : logger.factory.js
 *  ClassName  : logger.factory.js
 *  Modified   : 16012817
 */


/* @ngInject */
let loggerFactory = function ($log, toastr) {
    return {
        showToasts: true,

        error: error,
        info: info,
        success: success,
        warning: warning,

        // straight to console; bypass toastr
        log: $log.log
    };
    /////////////////////

    function error(message, data, title) {
        toastr.error(message, title);
        $log.error('Error: ' + message, data);
    }

    function info(message, data, title) {
        toastr.info(message, title);
        $log.info('Info: ' + message, data);
    }

    function success(message, data, title) {
        toastr.success(message, title);
        $log.info('Success: ' + message, data);
    }

    function warning(message, data, title) {
        toastr.warning(message, title);
        $log.warn('Warning: ' + message, data);
    }
};

export default loggerFactory;

