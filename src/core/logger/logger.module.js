/*
 * Copyright (c) 2016. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : debox-web-core
 *  Module     : debox-web-core
 *  File       : logger.module.js
 *  ClassName  : logger.module.js
 *  Modified   : 16012817
 */

import angular from 'angular';
import toastr from 'toastr';
import loggerFactory from './logger.factory';

export default angular
	.module('deboxCore.logger', [])
    .constant('toastr', toastr)
    .factory('logger', loggerFactory);
