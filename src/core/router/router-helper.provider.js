/*
 * Copyright (c) 2016. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : debox-web-core
 *  Module     : debox-web-core
 *  File       : router-helper.provider.js
 *  ClassName  : router-helper.provider.js
 *  Modified   : 16012817
 */
/* Help configure the state-base ui.router */
import angular from 'angular';

/* @ngInject */
let routerHelperProvider = function ($locationProvider, $stateProvider, $urlRouterProvider) {
	/* jshint validthis:true */
	var config = {
		docTitle: undefined,
		resolveAlways: {}
	};
	$locationProvider.html5Mode(true);
	this.configure = function (cfg) {
		angular.extend(config, cfg);
	};
	this.$get = RouterHelper;

	/* @ngInject */
	function RouterHelper($location, $scope, $state, logger) {
		var handlingStateChangeError = false;
		var hasOtherwise = false;
		var stateCounts = {
			errors: 0,
			changes: 0
		};
		var service = {
			configureStates: configureStates,
			getStates: getStates,
			stateCounts: stateCounts
		};
		init();
		return service;
		///////////////
		function configureStates(states, otherwisePath) {
			states.forEach(function (state) {
				state.config.resolve = angular.extend(state.config.resolve || {}, config.resolveAlways);
				addState(state);
			});
			if (otherwisePath && !hasOtherwise) {
				hasOtherwise = true;
				$urlRouterProvider.otherwise(otherwisePath);
			}
		}

		function handleRoutingErrors() {
			// Route cancellation:
			// On routing error, go to the dashboard.
			// Provide an exit clause if it tries to do it twice.
			$scope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
				if (handlingStateChangeError) {
					return;
				}
				stateCounts.errors++;
				handlingStateChangeError = true;
				var destination = (toState && (toState.title || toState.name || toState.loadedTemplateUrl)) || 'unknown target';
				var msg = 'Error routing to ' + destination + '. ' + (error.data || '') + '. <br/>' + (error.statusText || '') + ': ' + (error.status || '');
				logger.warning(msg, [toState]);
				$location.path('/f');
			});
		}

		function init() {
			handleRoutingErrors();
			updateDocTitle();
		}

		function getStates() {
			return $state.get();
		}

		function updateDocTitle() {
			$scope.$on('$stateChangeSuccess', function (event, toState) {
				stateCounts.changes++;
				handlingStateChangeError = false;
				$rootScope.title = config.docTitle + ' ' + (toState.title || ''); // data bind to <title>
			});
		}

		/**
		 *  Child states are defined via a `children` property.
		 *
		 * 1. Recursively calls itself for all descendant states, by traversing the `children` properties.
		 * 2. Converts all the state names to dot notation, of the form `grandfather.father.state`.
		 * 3. Sets `parent` property of the descendant states.
		 *
		 * @param {Object} state - A regular ui.router state object.
		 * @param opts
		 * @param {Array} [state.children] - An optional array of child states.
		 * @param {Object} [state.config] - An optional options object.
		 * @param {Boolean} [state.config.keepOriginalNames=false] An optional flag that
		 *     prevents conversion of names to dot notation if true.
		 * @param {Boolean} [state.config.siblingTraversal=false] An optional flag that
		 *     adds `nextSibling` and `previousSibling` properties when enabled
		 *
		 **/
		function addState(state, opts) {
			opts = {
				keepOriginalNames: false,
				siblingTraversal: false
			};
			if (angular.isDefined(state.config.keepOriginalNames)) {
				opts.keepOriginalNames = state.config.keepOriginalNames;
			}
			if (angular.isDefined(state.config.siblingTraversal)) {
				opts.siblingTraversal = state.config.siblingTraversal;
			}
			if (!opts.keepOriginalNames) {
				fixStateName(state);
			}
			$stateProvider.state(state.state, state.config);
			if (state.children && state.children.length) {
				state.children.forEach(function (childState) {
					childState.parent = state;
					addState(childState, opts);
				});
				if (opts.siblingTraversal) {
					addSiblings(state);
				}
			}
		}

		/**
		 * Converts the name of a state to dot notation, of the form `grandfather.father.state`.
		 * @param state
		 */
		function fixStateName(state) {
			if (state.parent) {
				state.state = (angular.isObject(state.parent) ? state.parent.state : state.parent) + '.' + state.state;
			}
		}

		function addSiblings(state) {
			state.children.forEach(function (childState, idx, array) {
				if (array[idx + 1]) {
					childState.nextSibling = array[idx + 1].state;
				}
				if (array[idx - 1]) {
					childState.previousSibling = array[idx - 1].state;
				}
			});
		}
	}
};

export default routerHelperProvider;
