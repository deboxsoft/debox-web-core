/*
 * Copyright (c) 2016. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : debox-web-core
 *  Module     : debox-web-core
 *  File       : router.module.js
 *  ClassName  : router.module.js
 *  Modified   : 16012817
 */

import angular from 'angular';
import routerHelperProvider from './router-helper.provider';

export default angular
	.module('deboxCore.router', [
	    'ui.router',
	    'deboxCore.logger'
	])
	.provider('routerHelper', routerHelperProvider);