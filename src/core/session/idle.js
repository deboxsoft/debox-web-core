/*
 * Copyright (c) 2016. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : debox-web-core
 *  Module     : debox-web-core
 *  File       : idle.js
 *  ClassName  : idle.js
 *  Modified   : 16049210
 */

import angular from 'angular';


/* @ngInject */
let idleProvider = function () {
    /**
     *  Sets the number of seconds a user can be idle before they are considered timed out.
     *  @param {Number|Boolean} seconds A positive number representing seconds OR 0 or false to disable this feature.
     */
    this.setTimeout = _setTimeout;
    this.setInterruptEvent = _setInterruptEvent;
    this.setIdle = _setIdle;
    this.autoResume = _autoResume;
    this.setEnableKeepAlive = _keepAlive;
    this.$get = idleService;

    ///////////////////////

    var options = {
        idle: 20 * 60, // in seconds (default is 20min)
        timeout: 30, // in seconds (default is 30sec)
        autoResume: 'idle', // lets events automatically resume (unsets idle state/resets warning)
        interrupt: 'mousemove keydown DOMMouseScroll mousewheel mousedown touchstart touchmove scroll',
        keepAlive: true
    };

    var state = {
        idle: null,
        timeout: null,
        idling: false,
        running: false,
        countdown: null
    };

    var IDLE_START_EVENT = 'IdleStartEvent',
        IDLE_END_EVENT = 'IdleEndEvent',
        IDLE_WARN_EVENT = 'IdleWarnEvent',
        IDLE_TIMEOUT_EVENT = 'IdleTimeoutEvent';

    var id = new Date().getTime();

	/* @ngInject */
    function idleService($interval, $log, $rootScope, $document, KeepAlive, Session, $window) {
        init();

        var svc = {
            _options: function () {
                return options;
            },
            _getNow: function () {
                return new Date().getTime();
            },
            getIdle: function () {
                return options.idle;
            },
            getTimeout: function () {
                return options.timeout;
            },
            setIdle: function (seconds) {
                _changeOption(_setIdle, seconds);
            },
            setTimeout: function (seconds) {
                _changeOption(_setTimeout, seconds);
            },
            isExpired: function () {
                var expired = Session.getItem('expiry');
                return expired && expired.time ? new Date(expired.time) : null;
            },
            isRunning: function () {
                return state.running;
            },
            isIdling: function () {
                return state.idling;
            },
            unwatch: function () {
                $interval.cancel(state.idle);
                $interval.cancel(state.timeout);

                state.running = false;
                state.idling = false;
                setExpiry(null);
                stopKeepAlive();
            },

            /**
             *
             * @param noExpiryUpdate
             */
            watch: function (noExpiryUpdate) {
                $interval.cancel(state.idle);
                $interval.cancel(state.timeout);

                // calculate the absolute expiry date, as added insurance against a browser sleeping or paused in the background
                var timeout = !options.timeout ? 0 : options.timeout;
                if (!noExpiryUpdate) {
                    setExpiry(new Date(this._getNow() + ((options.idle + timeout) * 1000)));
                }

                if (this.isIdling()) { // clears the idle state if currently idling
                    toggleIdling();
                }
                else if (!svc.isRunning()) { // if about to run, start keep alive
                    startKeepAlive();
                }

                state.running = true;
                state.idle = $interval(toggleIdling, options.idle * 1000, 0, false);
            },

            /**
             * reset idle
             * @param {boolean} noExpiryUpdate - tidak update storage session.
             */
            interrupt: function (noExpiryUpdate) {
                if (!svc.isRunning()) {
                    return;
                }
                if (options.timeout && this.isExpired()) {
                    timeout();
                    return;
                }

                // note: you can no longer auto resume once we exceed the expiry; you will reset state by calling watch() manually
                if (noExpiryUpdate || options.autoResume === 'idle' || (options.autoResume === 'notIdle' && !svc.isIdling())) {
                    watch(noExpiryUpdate);
                }
            },
            IDLE_START_EVENT: IDLE_START_EVENT,
            IDLE_STOP_EVENT: IDLE_END_EVENT,
            IDLE_WARN_EVENT: IDLE_WARN_EVENT,
            IDLE_TIMEOUT_EVENT: IDLE_TIMEOUT_EVENT
        };

        return svc;
        ///////////////

        function init() {
            $document.find('html').on(options.interrupt, function (event) {
                if (event.type === 'mousemove' && event.originalEvent && event.originalEvent.movementX === 0 && event.originalEvent.movementY === 0) {
                    return; // Fix for Chrome desktop notifications, triggering mousemove event.
                }

                /*
                 note:
                 webkit fires fake mousemove events when the user has done nothing, so the idle will never time out while the cursor is over the webpage
                 Original webkit bug report which caused this issue:
                 https://bugs.webkit.org/show_bug.cgi?id=17052
                 Chromium bug reports for issue:
                 https://code.google.com/p/chromium/issues/detail?id=5598
                 https://code.google.com/p/chromium/issues/detail?id=241476
                 https://code.google.com/p/chromium/issues/detail?id=317007
                 */
                if (event.type !== 'mousemove' || angular.isUndefined(event.movementX) || (event.movementX || event.movementY)) {
                    svc.interrupt();
                }
            });
            if ($window.addEventListener) {
                $window.addEventListener('storage', _wrap, false);
            }
            else {
                $window.attachEvent('onstorage', _wrap);
            }
        }

        function _wrap(event) {
            if (event.key === Session.getKeyNameLocalStorage('registry') && event.newValue && event.newValue !== event.oldValue) {
                var val = angular.fromJson(event.newValue);
                if (val.id === id) {
                    return;
                }
                svc.interrupt(true);
            }
        }

        function timeout() {
            stopKeepAlive();
            $interval.cancel(state.idle);
            $interval.cancel(state.timeout);
            state.idling = true;
            state.running = false;
            state.countdown = 0;

            $rootScope.$broadcast(IDLE_TIMEOUT_EVENT);
        }

        function setExpiry(date) {
            if (date) {
                Session.setItem('expiry', {id: id, time: date});
            } else {
                Session.removeItem('expiry');
            }
        }

        /**
         * start keep alive session
         */
        function startKeepAlive() {
            if (!options.keepAlive) {
                return;
            }

            if (svc.isRunning()) {
                KeepAlive.ping();
            }

            KeepAlive.start();
        }

        /**
         * stop keep alive session
         */
        function stopKeepAlive() {
            if (!options.keepAlive) {
                return;
            }

            KeepAlive.stop();
        }

        /**
         * toggle state.idling status
         */
        function toggleIdling() {
            state.idling = !svc.isIdling();

            if (svc.isIdling()) {
                $rootScope.$broadcast(IDLE_START_EVENT);
                stopKeepAlive();
                if (options.timeout) {
                    state.countdown = options.timeout;
                    countdown();
                    state.timeout = $interval(countdown, 1000, options.timeout, false);
                }

            } else {
                $rootScope.$broadcast(IDLE_END_EVENT);
                startKeepAlive();
            }

        }

        function countdown() {
            // check not called when no longer idling
            // possible with multiple tabs
            if (!svc.isIdling()) {
                return;
            }

            // countdown has expired, so signal timeout
            if (state.countdown <= 0) {
                timeout();
                return;
            }

            // countdown hasn't reached zero, so warn and decrement
            $rootScope.$broadcast(IDLE_WARN_EVENT, state.countdown);
            state.countdown--;
        }

        function _changeOption(fn, value) {
            svc.unwatch();
            fn(value);
            if (svc.isRunning()) {
                svc.watch();
            }
        }

    }

    ///////////////////

    /**
     * set idle
     * @param {number} seconds time in seconds.
     * @private
     */
    function _setIdle(seconds) {
        if (seconds <= 0) {
            throw new Error('Idle must be a value in seconds, greater than 0.');
        }

        options.idle = seconds;
    }

    function _setInterruptEvent(events) {
        options.interrupt = events;
    }

    function _setTimeout(seconds) {
        if (seconds === false) {
            options.timeout = 0;
        }
        else if (angular.isNumber(seconds) && seconds >= 0) {
            options.timeout = seconds;
        }
        else {
            throw new Error('Timeout must be zero or false to disable the feature, or a positive integer (in seconds) to enable it.');
        }
    }

    function _autoResume(value) {
        if (value === true) {
            options.autoResume = 'idle';
        }
        else if (value === false) {
            options.autoResume = 'off';
        }
        else {
            options.autoResume = value;
        }
    }

    /**
     *
     * @param {boolean} enabled
     * @private
     */
    function _keepAlive(enabled) {
        options.keepAlive = enabled === true;
    }
};

export default idleProvider;
