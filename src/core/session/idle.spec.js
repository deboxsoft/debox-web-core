/*
 * Copyright (c) 2016. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : debox-web-core
 *  Module     : debox-web-core
 *  File       : idle.spec.js
 *  ClassName  : idle.spec.js
 *  Modified   : 16023210
 */

import './session.module';
import 'angular-mocks';

/* jshint -W117, -W030 */
describe('test idle', function () {

    var idleProvider, keepAlive, $injector;


    // helpers
    beforeEach(function () {
        angular.mock.module('deboxCore.session', function (_idleProvider_) {
            idleProvider = _idleProvider_;
        });
        inject(function (_$injector_) {
            $injector = _$injector_;
        });
        keepAlive = {
            start: function () {
            },
            stop: function () {
            },
            ping: function () {
            }
        };
        spyOn(keepAlive, 'start');
        spyOn(keepAlive, 'stop');
        spyOn(keepAlive, 'ping');
    });

    describe('idleProvider', function () {
        var DEFAULT_EDURATION = 20 * 60 * 1000, DEFAULT_TIMEOUT = 30 * 1000;
        var session, $rootScope, $interval, IdleService;
        beforeEach(function () {
            session = $injector.get('session');
            $rootScope = $injector.get('$rootScope');
            $interval = $injector.get('$interval');
            IdleService = function (keppAlive) {
                if (angular.isDefined(keppAlive)) {
                    idleProvider.setEnableKeepAlive(keppAlive);
                }
                return $injector.invoke(idleProvider.$get, null, {
                    $interval: $interval,
                    $log: $injector.get('$log'),
                    $rootScope: $rootScope,
                    $document: $injector.get('$document'),
                    KeepAlive: keepAlive,
                    Session: session,
                    $window: $injector.get('$window')
                });
            };
        });


        it('setInterruptEvent() akan merubah nilai option interrupt', function () {
            expect(idleProvider).toBeDefined();
            idleProvider.setInterruptEvent('click');
            expect(new IdleService()._options().interrupt).toEqual('click');
        });

        it('setIdle(second) akan merubah nilai option idle', function () {
            expect(idleProvider).toBeDefined();
            idleProvider.setIdle(50);
            expect(new IdleService()._options().idle).toEqual(50);
        });

        it('setIdle(second) akan terjadi error kalau second bernilai 0 atau negatif', function () {
            expect(function () {
                idleProvider.setIdle(0);
            }).toThrowError();
            expect(function () {
                idleProvider.setIdle(-1);
            }).toThrowError();
        });

        it('setTimeout() akan menghasilkan nilai 0 jika input false', function () {
            idleProvider.setTimeout(false);
            expect(new IdleService()._options().timeout).toBe(0);
        });

        it('setTimeout() akan throw error jika input NaN', function () {
            expect(function () {
                idleProvider.setTimeout('timeout');
            }).toThrow();

        });

        describe('idle service event', function () {
            var idle;
            beforeEach(function () {
                session.removeItem('registry');
                idleProvider.setTimeout(3);
                idle = new IdleService();
            });

            it('setIdle() akan memperbarui options idle dan restart', function () {
                spyOn(idle, 'watch');
                spyOn(idle, 'unwatch');
                spyOn(idle, 'isRunning').and.callFake(function () {
                    return true;
                });

                idle.setIdle(10);
                expect(idle._options().idle).toBe(10);
                expect(idle.unwatch).toHaveBeenCalled();
                expect(idle.watch).toHaveBeenCalled();
            });

            it('getIdle() akan mengembalikan nilai setting idle', function () {
                idle.setIdle(10);
                expect(idle.getIdle()).toBe(10);
            });

            it('setTimeout() akan memperbarui options timeout dan restart', function () {
                spyOn(idle, 'watch');
                spyOn(idle, 'unwatch');
                spyOn(idle, 'isRunning').and.callFake(function () {
                    return true;
                });

                idle.setTimeout(10);
                expect(idle._options().timeout).toBe(10);
                expect(idle.unwatch).toHaveBeenCalled();
                expect(idle.watch).toHaveBeenCalled();
            });

            it('getTimeout() akan mengembalikan nilai setting timeout', function () {
                idle.setTimeout(100);
                expect(idle.getTimeout()).toBe(100);
            });

            it('watch() akan clear running dan start running', function () {
                spyOn($interval, 'cancel');
                expect(idle.isRunning()).toBe(false);
                idle.watch();
                expect($interval.cancel.calls.count()).toBe(2);
                expect(idle.isRunning()).toBe(true);
                expect(keepAlive.start).toHaveBeenCalled();
            });

            it('watch() with options keepAlive false', function () {
                idle = new IdleService(false);
                idle.watch();
                expect(keepAlive.start).not.toHaveBeenCalled();
                $interval.flush(DEFAULT_EDURATION);
                idle.watch();
            });

            it('watch() keepAlive.stop with option keepAlive true or default', function () {
                idle.watch();
                $interval.flush(DEFAULT_EDURATION);
                expect(keepAlive.stop).toHaveBeenCalled();
            });

            //
            // hack ::
            // it('body event listener ', function () {
            //    idle.watch();
            //    expect(idle.isRunning()).toBeTruthy();
            //    expect(idle.isIdling()).toBeFalsy();
            //    $interval.flush(DEFAULT_EDURATION);
            //    expect(idle.isRunning()).toBeFalsy();
            //    expect(idle.isIdling()).toBeTruthy();
            //
            //    //var spyEvent = spyOnEvent('body', 'click');
            //    $('body').click();
            //    //expect(spyEvent).toHaveBeenTriggered();
            //    //expect(idle.isIdling()).toBeFalsy();
            //});

            it('watch() should interrupt countdown', function () {
                spyOn($rootScope, '$broadcast');

                idle.watch();
                $interval.flush(DEFAULT_EDURATION);

                $interval.flush(1000);
                $rootScope.$digest();

                expect(idle.isIdling()).toBeTruthy();

                idle.watch();
                expect(idle.isIdling()).toBeFalsy();
            });

            it('watch() keepAlive.stop with option keepAlive false or default', function () {
                var idle = new IdleService(false);
                idle.watch();
                $interval.flush(DEFAULT_EDURATION);
                expect(keepAlive.stop).not.toHaveBeenCalled();
            });


            it('interrupt(true) tidak akan melakukan update session', function () {
                spyOn(session, 'setItem').and.callThrough();
                idle.interrupt(true);
                expect(session.setItem).not.toHaveBeenCalled();
            });


        });
    });

});
