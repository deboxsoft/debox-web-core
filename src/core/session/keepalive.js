/*
 * Copyright (c) 2016. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : debox-web-core
 *  Module     : debox-web-core
 *  File       : keepalive.js
 *  ClassName  : keepalive.js
 *  Modified   : 16012817
 */

import angular from 'angular';

let keepAliveProvider = function () {

	var options = {
		http: null,
		interval: 10 * 60
	};

	this.setHttp = setHttp;
	this.setInterval = _setInterval;

	/* @ngInject */
	this.$get = ['$rootScope', '$log', '$interval', '$http', function ($rootScope, $log, $interval, $http) {
		var REQUEST_EVENT = 'KeepaliveRequestEvent';
		var RESPONSE_EVENT = 'KeepaliveResponseEvent';
		var stopPing;

		return {
			setInterval: _setInterval,
			start: start,
			stop: stop,
			ping: ping,
			REQUEST_EVENT: REQUEST_EVENT,
			RESPONSE_EVENT: RESPONSE_EVENT,
			_options: options
		};

		///////////////
		/**
		 * start keepalive
		 */
		function start() {
			stop();
			stopPing = $interval(ping, options.interval * 1000);
			return stopPing;
		}

		/**
		 * stop keepalive
		 */
		function stop() {
			$interval.cancel(stopPing);
		}

		/**
		 * keep alive session server.
		 */
		function ping() {
			$rootScope.$broadcast(REQUEST_EVENT);

			if (angular.isObject(options.http)) {
				$http(options.http)
					.success(handleResponse)
					.error(handleResponse);
			}
		}

		function handleResponse(data, status) {
			$rootScope.$broadcast(RESPONSE_EVENT, data, status);
		}
	}];

	////////////////
	/**
	 * set config url.
	 *
	 * @param {String|Object} value value url
	 */
	function setHttp(value) {
		if (!value) {
			throw new Error('http harus string sebuah URL atau obyek konfigurasi request HTTP.');
		}
		if (angular.isString(value)) {
			value = {
				url: value,
				method: 'GET'
			};
		}

		value.cache = false;

		options.http = value;
	}

	/**
	 * set config interval.
	 * @param {number} seconds
	 */
	function _setInterval(seconds) {
		seconds = parseInt(seconds);

		if (isNaN(seconds) || seconds <= 0) {
			throw new Error('Interval harus dalam detik dan besarannya lebih dari 0');
		}
		options.interval = seconds;
	}
};

export default keepAliveProvider;


