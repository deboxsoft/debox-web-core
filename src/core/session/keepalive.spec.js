/*
 * Copyright (c) 2016. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : debox-web-core
 *  Module     : debox-web-core
 *  File       : keepalive.spec.js
 *  ClassName  : keepalive.spec.js
 *  Modified   : 16023210
 */
import angular from 'angular';
import './session.module';
import 'angular-mocks';


describe('keepalive provider', function () {
    var keepAliveProvider, $rootScope;
    beforeEach(function () {
        angular.mock.module('deboxCore.session', function (_keepAliveProvider_) {
            keepAliveProvider = _keepAliveProvider_;
        });
        inject(function(_$rootScope_){
            $rootScope = _$rootScope_;
        });
        spyOn($rootScope, '$broadcast');
    });

    describe('test connect to server', function () {
        var _url = 'http://deboxtest/ping';
        beforeEach(function () {
            keepAliveProvider.setHttp(_url);
        });
        it('setHttp(url) should error if error null', function(){
            expect(function(){
                keepAliveProvider.setHttp();
            }).toThrowError();
        });
        it('test setHttp()', inject(function (_keepAlive_) {
            var _httpExpected = {
                url : _url,
                method: 'GET',
                cache: false
            };
            expect(_keepAlive_._options.http).toEqual(_httpExpected);
        }));

        it('should keep alive session server broadcast KeepAlive.RESPONSE_EVENT.', inject(function(_keepAlive_, _$httpBackend_){
            var pingRequest = _$httpBackend_.when('GET', _url).respond(200, '');
            _$httpBackend_.expectGET(_url);
            _keepAlive_.ping();
            expect($rootScope.$broadcast).toHaveBeenCalledWith(_keepAlive_.REQUEST_EVENT);
            _$httpBackend_.flush();
            expect($rootScope.$broadcast).toHaveBeenCalledWith(_keepAlive_.RESPONSE_EVENT, '', 200);
            _$httpBackend_.verifyNoOutstandingExpectation();
            _$httpBackend_.verifyNoOutstandingRequest();

        }));
    });

    it('setInterval should error input', inject(function () {
        expect(function () {
            keepAliveProvider.setInterval(-1);
        }).toThrowError();
        expect(function () {
            keepAliveProvider.setInterval(1);
        }).not.toThrowError();
    }));

    it('cek interval time', inject(function (_keepAlive_, _$interval_) {
        _keepAlive_.start();
        expect($rootScope.$broadcast).not.toHaveBeenCalledWith(_keepAlive_.REQUEST_EVENT);
        _$interval_.flush((_keepAlive_._options.interval * 1000) + 10);
        expect($rootScope.$broadcast).toHaveBeenCalledWith(_keepAlive_.REQUEST_EVENT);
    }));


});
