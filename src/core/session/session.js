/*
 * Copyright (c) 2016. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : debox-web-core
 *  Module     : debox-web-core
 *  File       : session.js
 *  ClassName  : session.js
 *  Modified   : 16012817
 */

import angular from 'angular';


/* @ngInject */
var sessionFactory = function ($window) {
	var _prefix = 'dboxStorage';
	var sessionStorage = getStorage();

	return {

		getKeyNameLocalStorage: getKeyNameLocalStorage,
		setItem: sessionStorage._setItem,
		getItem: sessionStorage._getItem,
		removeItem: sessionStorage._removeItem,
		storage: sessionStorage
	};
	////////////////

	/**
	 * render name key for $window.localStorage
	 * @param key
	 * @returns {string}
	 */
	function getKeyNameLocalStorage(key) {
		return _prefix + '.' + key;
	}

	// Safari, in Private Browsing Mode, looks like it supports localStorage but all calls to setItem
	// throw QuotaExceededError. We're going to detect this and just silently drop any calls to setItem
	// to avoid the entire page breaking, without having to do a check at each usage of Storage.
	function getStorage() {
		try {
			var storage = new LocalStorage();
			storage._setItem('test', '');
			storage._removeItem('test');
			return storage;
		} catch (err) {
			return new MemoryStorage();
		}
	}

	////
	function MemoryStorage() {
		var storageMap = {};

		this._setItem = function (key, value) {
			storageMap[key] = value;
		};

		this._getItem = function (key) {

			if (angular.isDefined(storageMap[key])) {
				return storageMap[key];
			}
			return null;
		};

		this._removeItem = function (key) {
			storageMap[key] = undefined;
		};
	}

	function LocalStorage() {
		var storage = $window.localStorage;

		this._setItem = function (key, value) {
			storage.setItem(getKeyNameLocalStorage(key), angular.toJson(value));
		};

		this._getItem = function (key) {
			return angular.fromJson(storage.getItem(getKeyNameLocalStorage(key)));
		};

		this._removeItem = function (key) {
			storage.removeItem(getKeyNameLocalStorage(key));
		};
	}
};

export default sessionFactory;
