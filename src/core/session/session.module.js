/*
 * Copyright (c) 2016. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : debox-web-core
 *  Module     : debox-web-core
 *  File       : session.module.js
 *  ClassName  : session.module.js
 *  Modified   : 16012817
 */

import angular from 'angular';
import IdleProvider from './idle';
import KeepAliveProvider from './keepalive';
import Session from './session';


export default angular
    .module('deboxCore.session', [])
    .provider('idle', IdleProvider)
    .provider('keepAlive', KeepAliveProvider)
    .factory('session', Session);
