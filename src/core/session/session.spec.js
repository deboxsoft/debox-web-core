/*
 * Copyright (c) 2016. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : debox-web-core
 *  Module     : debox-web-core
 *  File       : session.spec.js
 *  ClassName  : session.spec.js
 *  Modified   : 16012817
 */
import angular from 'angular';
import './session.module';
import 'angular-mocks';


describe('session service testing', function () {
	var $window;
	beforeEach(function () {
		angular.mock.module('deboxCore.session');
		inject(function (_$window_) {
			$window = _$window_;
		});

	});

	describe('Memory storage', function () {
		beforeEach(function () {
			spyOn($window.localStorage, 'setItem').and.throwError();
		});

		it('should not support localstorage switch to MemoryStorage', inject(function (_session_) {
			expect($window.localStorage.setItem).toHaveBeenCalled();
			expect(_session_.storage.constructor.name).toEqual('MemoryStorage');
		}));

		it('test CRUD memory storage', inject(function (_session_) {
			_session_.setItem('test', '123');
			expect(_session_.getItem('test')).toEqual('123');
			_session_.removeItem('test');
			expect(_session_.getItem('test')).toBeNull();
		}));
	});
	it('test localStorage', inject(function (_session_) {
		_session_.setItem('test', '123');
		var key = _session_.getKeyNameLocalStorage('test');
		expect(angular.fromJson($window.localStorage.getItem(key))).toEqual('123');
		expect(_session_.getItem('test')).toEqual('123');
		_session_.removeItem('test');
		expect(_session_.getItem('test')).toBeNull();
	}));
});
