/*
 * Copyright (c) 2016. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : debox-web-core
 *  Module     : debox-web-core
 *  File       : index.js
 *  ClassName  : index.js
 *  Modified   : 160410210
 */


var testsContext = require.context("../src", true, /\.spec\.js$/);
testsContext.keys().forEach(testsContext);

var appContext = require.context("../src", true, /debox\-core\.module\.js$/);
appContext.keys().forEach(appContext);
