/* eslint-env node */
/* eslint-disable angular/log */

var AnnotatePlugin = require('ng-annotate-webpack-plugin');
var path = require('path');
var _ = require('lodash');
var status = process.env.NODE_ENV || 'development';
var isDevel = status === 'development';
process.env.CI = status === 'test:ci';
var exclude = /node_modules/;
var packageJson = require('./package.json');
var webpack = require('webpack');
var WebpackNotifierPlugin = require('webpack-notifier');


var configFns = {
	development: getDevConfig,
	production: getProdConfig,
	test: getTestConfig,
	'test:ci': getTestCIConfig
};

var config = configFns[status]();
addCommonsPlugin();

console.log('building version %s in %s mode', packageJson.version, status);

module.exports = config;


//************************************* function ***************************///

function getDevConfig() {
	var devConfig = {
		context: __dirname + '/src',

		entry: {
			main: './core/debox-core.module.js'
		},

		output: {
			path: 'dist',
			filename: '[name].js',
			chunkFilename: '[name]-[chunkhash].js'
		},

		devtool: 'source-map',

		plugins: [],

		module: {
			loaders: _.union(
				getJavaScriptLoaders()
			)
		},

		externals: {
			'jquery': 'jquery',
			'angular': 'angular',
			'toastr': 'toastr',
			'angular-mocks': 'angular-mocks'
		},

		eslint: {
			emitError: false,
			failOnError: false,
			failOnWarning: false,
			quiet: true
		}
	};

	if (process.env.CI !== 'true') {
		devConfig.plugins = [
			new WebpackNotifierPlugin()
		];
	}

	return devConfig;
}

function getProdConfig(noUglify) {
	var prodConfig = _.merge({}, getDevConfig(), {
		output: {
			filename: '[name].min.js'
		},
		devtool: 'source-map',
		eslint: {
			emitError: true,
			failOnError: true
		}
	});

	prodConfig.plugins = [
		new webpack.optimize.DedupePlugin(),
		new webpack.optimize.OccurenceOrderPlugin(),
		new webpack.optimize.AggressiveMergingPlugin()
	];

	// This plugin minifies all the Javascript code of the final bundle
	if (process.env.MIN) {
		prodConfig.plugins.push(new webpack.optimize.UglifyJsPlugin({
			mangle: true,
			compress: {
				warnings: false // Suppress uglification warnings
			}
		}));
	}

	// allow getting rid of the UglifyJsPlugin
	// https://github.com/webpack/webpack/issues/1079
	if (!noUglify) {
		prodConfig.plugins.push(new webpack.optimize.UglifyJsPlugin({
			compress: {warnings: false}
		}));
	}
	return prodConfig;
}

function getTestConfig() {
	var config = getDevConfig();
	config.externals = {};
	return _.merge({}, config, {
		entry: '../test/index.js'
	});
}

function getTestCIConfig() {
	var noUglify = true;
	var config = getProdConfig(noUglify);
	config.externals = {};
	return _.merge({}, config, {
		entry: '../test/index.js',
		module: {
			postLoaders: [
				// we do this because we don't want the tests uglified
				{test: /\.js$/, loader: 'uglify', exclude: /\.spec\.js$|\.mock\.js$/}
			]
		},
		'uglify-loader': {
			compress: {warnings: false}
		}
	});
}

// loader
function getJavaScriptLoaders() {
	if (status.indexOf('test') === -1 && process.env.COVERAGE !== 'true') {
		return [
			{
				test: /\.js$/,
				loaders: ['babel', 'eslint'],
				exclude: exclude
			}
		];
	} else {
		return [
			{
				test: /\.spec\.js$|\.mock\.js$/, // include only mock and test files
				loaders: ['babel', 'eslint'],
				exclude: exclude
			},
			{
				test: /\.js$/,
				loaders: ['isparta'],
				exclude: /node_modules|\.spec.js$|\.mock\.js$/ // exclude node_modules and test files
			}
		];
	}
}


/* commons plugin */
function addCommonsPlugin() {
	config.plugins = config.plugins || [];
	config.plugins.unshift(new webpack.BannerPlugin(getBanner(), {raw: true}));
	config.plugins.unshift(new AnnotatePlugin({
		add: true
	}));
	//noinspection Eslint
	config.plugins.unshift(new webpack.DefinePlugin({
		__SERVER__: isDevel,
		__DEVELOPMENT__: isDevel,
		__DEVTOOLS__: isDevel,
		VERSION: JSON.stringify(packageJson.version),
		'process.env': {
			BABEL_ENV: JSON.stringify(process.env.NODE_ENV),
			NODE_ENV: JSON.stringify(process.env.NODE_ENV)
		}
	}));
}

function getBanner() {
	return '/*! \n'
		+ '* ' + packageJson.name + 'v' + packageJson.version + ' \n'
		+ '  Copyright 2015-' + new Date().getFullYear() + 'by Deboxsoft.\n'
		+ '* ' + 'maintainers by ' +( [packageJson.maintainers[0].name]).join(', ') +
		'\n';
}
